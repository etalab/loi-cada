# Loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal

  - [Titre Ier : De la liberté d'accès aux documents administratifs et de la réutilisation des informations publiques](titre_ier)
  - [Titre V : Dispositions d'ordre social.](titre_v)
  - [Titre VI : Dispositions intéressant le code du travail.](titre_vi)
  - [Titre VII : Dispositions intéressant le code de la nationalité.](titre_vii)
  - [Titre VIII : Dispositions d'ordre fiscal et financier.](titre_viii)
  - [Titre IX : Dispositions diverses.](titre_ix)
