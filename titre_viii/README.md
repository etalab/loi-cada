# Titre VIII : Dispositions d'ordre fiscal et financier.

  - [Article 56](article_56.md)
  - [Article 57](article_57.md)
  - [Article 58](article_58.md)
