# Titre VI : Dispositions intéressant le code du travail.

  - [Article 50](article_50.md)
  - [Article 51](article_51.md)
  - [Article 52](article_52.md)
