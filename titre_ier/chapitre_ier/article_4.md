# Article 4

L'accès aux documents administratifs s'exerce, au choix du demandeur et dans la limite des possibilités techniques de l'administration :

a) Par consultation gratuite sur place, sauf si la préservation du document ne le permet pas ;

b) Sous réserve que la reproduction ne nuise pas à la conservation du document, par la délivrance d'une copie sur un support identique à celui utilisé par l'administration ou compatible avec celui-ci et aux frais du demandeur, sans que ces frais puissent excéder le coût de cette reproduction, dans des conditions prévues par décret ;

c) Par courrier électronique et sans frais lorsque le document est disponible sous forme électronique.
