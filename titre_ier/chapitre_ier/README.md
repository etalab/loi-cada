# Chapitre Ier : De la liberté d'accès aux documents administratifs.

  - [Article 1](article_1.md)
  - [Article 2](article_2.md)
  - [Article 3](article_3.md)
  - [Article 4](article_4.md)
  - [Article 6](article_6.md)
  - [Article 7](article_7.md)
  - [Article 8](article_8.md)
  - [Article 9](article_9.md)
