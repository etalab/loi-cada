# Article 1

Le droit de toute personne à l'information est précisé et garanti par les dispositions des chapitres Ier, III et IV du présent titre en ce qui concerne la liberté d'accès aux documents administratifs.

Sont considérés comme documents administratifs, au sens des chapitres Ier, III et IV du présent titre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, directives, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions et décisions.

Les actes et documents produits ou reçus par les assemblées parlementaires sont régis par l'ordonnance n° 58-1100 du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires.
