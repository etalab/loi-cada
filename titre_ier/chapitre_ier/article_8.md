# Article 8

Sauf disposition prévoyant une décision implicite de rejet ou un accord tacite, toute décision individuelle prise au nom de l'Etat, d'une collectivité territoriale, d'un établissement public ou d'un organisme, fût-il de droit privé, chargé de la gestion d'un service public, n'est opposable à la personne qui en fait l'objet que si cette décision lui a été préalablement notifiée.
