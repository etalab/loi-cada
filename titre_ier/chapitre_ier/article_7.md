# Article 7

Font l'objet d'une publication les directives, les instructions, les circulaires, ainsi que les notes et réponses ministérielles qui comportent une interprétation du droit positif ou une description des procédures administratives.

Les administrations mentionnées à l'article 1er peuvent en outre rendre publics les autres documents administratifs qu'elles produisent ou reçoivent.

Toutefois, sauf dispositions législatives contraires, les documents administratifs qui comportent des mentions entrant dans le champ d'application de l'article 6 ou, sans préjudice de l'article 13, des données à caractère personnel ne peuvent être rendus publics qu'après avoir fait l'objet d'un traitement afin d'occulter ces mentions ou de rendre impossible l'identification des personnes qui y sont nommées.

Un décret en Conseil d'Etat pris après avis de la commission mentionnée au chapitre III précise les modalités d'application du premier alinéa du présent article.
