# Article 6

I.-Ne sont pas communicables :

1° Les avis du Conseil d'Etat et des juridictions administratives, les documents de la Cour des comptes mentionnés à l'article L. 141-10 du code des juridictions financières et les documents des chambres régionales des comptes mentionnés à l'article L. 241-6 du même code, les documents élaborés ou détenus par l'Autorité de la concurrence dans le cadre de l'exercice de ses pouvoirs d'enquête, d'instruction et de décision, les documents élaborés ou détenus par la Haute Autorité pour la transparence de la vie publique dans le cadre des missions prévues à l'article 20 de la loi n° 2013-907 du 11 octobre 2013 relative à la transparence de la vie publique, les documents préalables à l'élaboration du rapport d'accréditation des établissements de santé prévu à l'article L. 6113-6 du code de la santé publique, les documents préalables à l'accréditation des personnels de santé prévue à l'article L. 1414-3-3 du code de la santé publique, les rapports d'audit des établissements de santé mentionnés à l'article 40 de la loi n° 2000-1257 du 23 décembre 2000 de financement de la sécurité sociale pour 2001 et les documents réalisés en exécution d'un contrat de prestation de services exécuté pour le compte d'une ou de plusieurs personnes déterminées ;

2° Les autres documents administratifs dont la consultation ou la communication porterait atteinte :

a) Au secret des délibérations du Gouvernement et des autorités responsables relevant du pouvoir exécutif ;

b) Au secret de la défense nationale ;

c) A la conduite de la politique extérieure de la France ;

d) A la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes ;

e) A la monnaie et au crédit public ;

f) Au déroulement des procédures engagées devant les juridictions ou d'opérations préliminaires à de telles procédures, sauf autorisation donnée par l'autorité compétente ;

g) A la recherche, par les services compétents, des infractions fiscales et douanières ;

h) Ou, sous réserve de l'article L. 124-4 du code de l'environnement, aux autres secrets protégés par la loi ;

II.-Ne sont communicables qu'à l'intéressé les documents administratifs :

-dont la communication porterait atteinte à la protection de la vie privée, au secret médical et au secret en matière commerciale et industrielle ;

-portant une appréciation ou un jugement de valeur sur une personne physique, nommément désignée ou facilement identifiable ;

-faisant apparaître le comportement d'une personne, dès lors que la divulgation de ce comportement pourrait lui porter préjudice.

Les informations à caractère médical sont communiquées à l'intéressé, selon son choix, directement ou par l'intermédiaire d'un médecin qu'il désigne à cet effet, dans le respect des dispositions de l'article L. 1111-7 du code de la santé publique.

III.-Lorsque la demande porte sur un document comportant des mentions qui ne sont pas communicables en application du présent article mais qu'il est possible d'occulter ou de disjoindre, le document est communiqué au demandeur après occultation ou disjonction de ces mentions.

Les documents administratifs non communicables au sens du présent chapitre deviennent consultables au terme des délais et dans les conditions fixés par les articles L. 213-1 et L. 213-2 du code du patrimoine. Avant l'expiration de ces délais et par dérogation aux dispositions du présent article, la consultation de ces documents peut être autorisée dans les conditions prévues par l'article L. 213-3 du même code.
