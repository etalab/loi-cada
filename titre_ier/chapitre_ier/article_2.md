# Article 2

Sous réserve des dispositions de l'article 6, les autorités mentionnées à l'article 1er sont tenues de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande, dans les conditions prévues par le présent titre.

Le droit à communication ne s'applique qu'à des documents achevés.

Le droit à communication ne concerne pas les documents préparatoires à une décision administrative tant qu'elle est en cours d'élaboration. Cependant, les avis, prévus par les textes législatifs ou réglementaires, au vu desquels est prise une décision rendue sur une demande tendant à bénéficier d'une décision individuelle créatrice de droits, sont communicables à l'auteur de cette demande dès leur envoi à l'autorité compétente pour statuer sur la demande. Lorsque les motifs de l'avis n'y figurent pas, ceux-ci doivent être également communiqués au demandeur en cas d'avis défavorable.

Par dérogation aux dispositions de l'alinéa précédent, les avis qui se prononcent sur les mérites comparés de deux ou plusieurs demandes dont l'administration a été saisie ne sont pas communicables tant que la décision administrative qu'ils préparent n'a pas été prise.

Le droit à communication ne s'exerce plus lorsque les documents font l'objet d'une diffusion publique.

Le dépôt aux archives publiques des documents administratifs communicables aux termes du présent chapitre ne fait pas obstacle au droit à communication à tout moment desdits documents.

Lorsqu'une administration mentionnée à l'article 1er est saisie d'une demande de communication portant sur un document administratif qu'elle ne détient pas mais qui est détenu par une autre administration mentionnée au même article, elle la transmet à cette dernière et en avise l'intéressé.

Lorsqu'une administration mentionnée à l'article 1er, ou la commission d'accès aux documents administratifs, est saisie d'une demande de communication d'un document administratif susceptible de relever de plusieurs des régimes d'accès mentionnés aux articles 20 et 21 de la présente loi, il lui appartient de l'examiner d'office au regard de l'ensemble de ces régimes, à l'exception du régime organisé par l'article L. 213-3 du code du patrimoine.

L'administration n'est pas tenue de donner suite aux demandes abusives, en particulier par leur nombre, leur caractère répétitif ou systématique.
