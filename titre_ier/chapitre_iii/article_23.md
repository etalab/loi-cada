# Article 23

La commission comprend onze membres :

a) Un membre du Conseil d'Etat, d'un grade au moins égal à celui de conseiller, président, un magistrat de la Cour de cassation et un magistrat de la Cour des comptes en activité ou honoraire, désignés respectivement par le vice-président du Conseil d'Etat, le premier président de la Cour de cassation et le premier président de la Cour des comptes ;

b) Un député et un sénateur, désignés respectivement par le président de l'Assemblée nationale et le président du Sénat ;

c) Un élu d'une collectivité territoriale, désigné par le président du Sénat ;

d) Un professeur de l'enseignement supérieur, en activité ou honoraire, proposé par le président de la commission ;

e) Une personnalité qualifiée en matière d'archives, proposée par le directeur des Archives de France ;

f) Une personnalité qualifiée en matière de protection des données à caractère personnel, proposée par le président de la Commission nationale de l'informatique et des libertés ;

g) Une personnalité qualifiée en matière de concurrence et de prix, proposée par le président de l'Autorité de la concurrence ;

h) Une personnalité qualifiée en matière de diffusion publique d'informations.

Un suppléant est désigné dans les mêmes conditions pour chacun des membres.

Les membres de la commission sont nommés par décret du Premier ministre. Leur mandat est, à l'exception de ceux mentionnés aux b et c, qui siègent pour la durée du mandat au titre duquel ils ont été désignés, d'une durée de trois ans. Ce mandat est renouvelable.

La commission comprend en outre, avec voix consultative, le Défenseur des droits ou son représentant.

Un commissaire du Gouvernement, désigné par le Premier ministre, siège auprès de la commission et assiste, sauf lorsqu'elle se prononce en application des dispositions des articles 18 et 22, à ses délibérations.

En cas de partage égal des voix, celle du président de la commission est prépondérante.

Un décret en Conseil d'Etat détermine les modalités de fonctionnement de la commission. Il fixe notamment les cas et les conditions dans lesquels la commission peut délibérer en formation restreinte.
