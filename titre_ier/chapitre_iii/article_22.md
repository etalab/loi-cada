# Article 22

La commission, lorsqu'elle est saisie par une administration mentionnée à l'article 1er, peut, au terme d'une procédure contradictoire, infliger à l'auteur d'une infraction aux prescriptions du chapitre II les sanctions prévues par l'article 18.
