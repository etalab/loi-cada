# Article 20

La commission d'accès aux documents administratifs est une autorité administrative indépendante.

Elle est chargée de veiller au respect de la liberté d'accès aux documents administratifs et aux archives publiques ainsi qu'à l'application du chapitre II relatif à la réutilisation des informations publiques dans les conditions prévues par le présent titre et par le titre Ier du livre II du code du patrimoine.

Elle émet des avis lorsqu'elle est saisie par une personne à qui est opposé un refus de communication d'un document administratif en application du chapitre Ier, un refus de consultation ou de communication des documents d'archives publiques, à l'exception des documents mentionnés au c de l'article L. 211-4 du code du patrimoine et des actes et documents produits ou reçus par les assemblées parlementaires, ou une décision défavorable en matière de réutilisation d'informations publiques.

La saisine pour avis de la commission est un préalable obligatoire à l'exercice d'un recours contentieux.
