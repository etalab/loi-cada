# Chapitre III : La commission d'accès aux documents administratifs.

  - [Article 20](article_20.md)
  - [Article 21](article_21.md)
  - [Article 22](article_22.md)
  - [Article 23](article_23.md)
