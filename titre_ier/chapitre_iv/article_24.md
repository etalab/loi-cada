# Article 24

Un décret en Conseil d'Etat, pris après avis de la commission d'accès aux documents administratifs, fixe les cas et les conditions dans lesquels les administrations mentionnées à l'article 1er sont tenues de désigner une personne responsable de l'accès aux documents et des questions relatives à la réutilisation des informations publiques.
