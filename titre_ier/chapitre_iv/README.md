# Chapitre IV : Dispositions communes.

  - [Article 24](article_24.md)
  - [Article 25](article_25.md)
  - [Article 26](article_26.md)
  - [Article 27](article_27.md)
  - [Article 28](article_28.md)
  - [Article 29](article_29.md)
