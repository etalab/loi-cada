# Article 10

Les informations figurant dans des documents produits ou reçus par les administrations mentionnées à l'article 1er, quel que soit le support, peuvent être utilisées par toute personne qui le souhaite à d'autres fins que celles de la mission de service public pour les besoins de laquelle les documents ont été produits ou reçus. Les limites et conditions de cette réutilisation sont régies par le présent chapitre, même si ces informations ont été obtenues dans le cadre de l'exercice du droit d'accès aux documents administratifs régi par le chapitre Ier.

Ne sont pas considérées comme des informations publiques, pour l'application du présent chapitre, les informations contenues dans des documents :

a) Dont la communication ne constitue pas un droit en application du chapitre Ier ou d'autres dispositions législatives, sauf si ces informations font l'objet d'une diffusion publique ;

b) Ou produits ou reçus par les administrations mentionnées à l'article 1er dans l'exercice d'une mission de service public à caractère industriel ou commercial ;

c) Ou sur lesquels des tiers détiennent des droits de propriété intellectuelle.

L'échange d'informations publiques entre les autorités mentionnées à l'article 1er, aux fins de l'exercice de leur mission de service public, ne constitue pas une réutilisation au sens du présent chapitre.
