# Article 15

La réutilisation d'informations publiques peut donner lieu au versement de redevances.

Pour l'établissement des redevances, l'administration qui a produit ou reçu les documents contenant des informations publiques susceptibles d'être réutilisées tient compte des coûts de mise à disposition des informations, notamment, le cas échéant, du coût d'un traitement permettant de les rendre anonymes.

L'administration peut aussi tenir compte des coûts de collecte et de production des informations et inclure dans l'assiette de la redevance une rémunération raisonnable de ses investissements comprenant, le cas échéant, une part au titre des droits de propriété intellectuelle. Dans ce cas, l'administration doit s'assurer que les redevances sont fixées de manière non discriminatoire et que leur produit total, évalué sur une période comptable appropriée en fonction de l'amortissement des investissements, ne dépasse pas le total formé, d'une part, des coûts de collecte, de production et de mise à disposition des informations et, d'autre part, le cas échéant, de la rémunération définie au présent alinéa.

Lorsque l'administration qui a produit ou reçu des documents contenant des informations publiques utilise ces informations dans le cadre d'activités commerciales, elle ne peut en facturer la réutilisation aux autres opérateurs à un coût supérieur à celui qu'elle s'impute, ni leur imposer des conditions moins favorables que celles qu'elle s'applique à elle-même.
