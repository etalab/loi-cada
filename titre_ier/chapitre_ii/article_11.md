# Article 11

Par dérogation au présent chapitre, les conditions dans lesquelles les informations peuvent être réutilisées sont fixées, le cas échéant, par les administrations mentionnées aux a et b du présent article lorsqu'elles figurent dans des documents produits ou reçus par :

a) Des établissements et institutions d'enseignement et de recherche ;

b) Des établissements, organismes ou services culturels.
