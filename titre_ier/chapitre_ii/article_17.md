# Article 17

Les administrations qui produisent ou détiennent des informations publiques tiennent à la disposition des usagers un répertoire des principaux documents dans lesquels ces informations figurent.

Les conditions de réutilisation des informations publiques, ainsi que les bases de calcul retenues pour la fixation du montant des redevances, sont communiquées, par les administrations qui ont produit ou détiennent ces informations, à toute personne qui en fait la demande.
