# Article 16

Lorsqu'elle est soumise au paiement d'une redevance, la réutilisation d'informations publiques donne lieu à la délivrance d'une licence.

Cette licence fixe les conditions de la réutilisation des informations publiques. Ces conditions ne peuvent apporter de restrictions à la réutilisation que pour des motifs d'intérêt général et de façon proportionnée. Elles ne peuvent avoir pour objet ou pour effet de restreindre la concurrence.

Les administrations qui élaborent ou détiennent des documents contenant des informations publiques pouvant être réutilisées dans les conditions prévues au présent article sont tenues de mettre préalablement des licences types, le cas échéant par voie électronique, à la disposition des personnes intéressées par la réutilisation de ces informations.

Les conditions dans lesquelles une offre de licence est proposée au demandeur sont fixées par voie réglementaire.
