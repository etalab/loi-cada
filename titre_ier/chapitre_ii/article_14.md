# Article 14

La réutilisation d'informations publiques ne peut faire l'objet d'un droit d'exclusivité accordé à un tiers, sauf si un tel droit est nécessaire à l'exercice d'une mission de service public.

Le bien-fondé de l'octroi d'un droit d'exclusivité fait l'objet d'un réexamen périodique au moins tous les trois ans.
