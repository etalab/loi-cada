# Article 18

Toute personne réutilisant des informations publiques en violation des prescriptions mentionnées aux deuxième et troisième alinéas du présent article est passible d'une amende prononcée par la commission mentionnée au chapitre III.

Le montant maximum de l'amende est égal à celui prévu par l'article 131-13 du code pénal pour les contraventions de 5e classe lorsque des informations publiques ont été réutilisées à des fins non commerciales en méconnaissance des dispositions de l'article 12 ou des conditions de réutilisation prévues par une licence délivrée à cet effet ou en violation de l'obligation d'obtention d'une licence.

Lorsque des informations publiques ont été réutilisées à des fins commerciales en méconnaissance des dispositions de l'article 12 ou des conditions de réutilisation prévues par une licence délivrée à cet effet ou en violation de l'obligation d'obtention d'une licence, le montant de l'amende est proportionné à la gravité du manquement commis et aux avantages tirés de ce manquement.

Pour l'application du troisième alinéa, le montant de l'amende prononcée pour sanctionner un premier manquement ne peut excéder 150 000 Euros. En cas de manquement réitéré dans les cinq années à compter de la date à laquelle la sanction précédemment prononcée est devenue définitive, il ne peut excéder 300 000 Euros ou, s'agissant d'une entreprise, 5 % du chiffre d'affaires hors taxes du dernier exercice clos dans la limite de 300 000 Euros.

La commission mentionnée au chapitre III peut, à la place ou en sus de l'amende, interdire à l'auteur d'une infraction la réutilisation d'informations publiques pendant une durée maximale de deux ans. Cette durée peut être portée à cinq ans en cas de récidive dans les cinq ans suivant le premier manquement.

La commission peut également ordonner la publication de la sanction aux frais de celui qui en est l'objet selon des modalités fixées par décret en Conseil d'Etat.

Les amendes sont recouvrées comme les créances de l'Etat étrangères à l'impôt et au domaine.
