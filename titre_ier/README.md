# Titre Ier : De la liberté d'accès aux documents administratifs et de la réutilisation des informations publiques

  - [Chapitre Ier : De la liberté d'accès aux documents administratifs.](chapitre_ier)
  - [Chapitre II : De la réutilisation des informations publiques.](chapitre_ii)
  - [Chapitre III : La commission d'accès aux documents administratifs.](chapitre_iii)
  - [Chapitre IV : Dispositions communes.](chapitre_iv)
